const GA_CATEGORY = "generator";

// Dependent on embed.css
const MAX_WIDGET_WIDTH = 500;
const MAX_WIDGET_HEIGHT = 640;
const HEADER_HEIGHT = 56;
const HEADER_MARGIN = 16;
const CONTENT_HEIGHT = 208;
const BORDER_THICKNESS = 1;

const COMPACT_WIDGET_SIZE = HEADER_HEIGHT + CONTENT_HEIGHT + BORDER_THICKNESS * 2 - HEADER_MARGIN;
const MAX_MEDIA_HEIGHT = MAX_WIDGET_HEIGHT - COMPACT_WIDGET_SIZE + HEADER_MARGIN;

window.postId = null;

window.onload = function() {
	var postId = getParameterByName("postId");
	if (postId) {
		document.getElementById("post_input").value = postId;
		generate();
	}
}

function generate() {
	var postInput = document.getElementById("post_input").value;

	var postIdPattern = /^\d+$/;
	if (postIdPattern.test(postInput)) {
		generateWithId(postInput);
		ga("send", "event", GA_CATEGORY, "generate with id", postInput);
		return;
	}

	var postUrlPattern = /^(?:https?:\/\/)?(?:www\.)?dcard\.(?:cc|tw)\/f\/[^\s]+\/p\/(\d+).*$/;
	if (postUrlPattern.test(postInput)) {
		generateWithId(postUrlPattern.exec(postInput)[1]);
		ga("send", "event", GA_CATEGORY, "generate with url", postInput);
		return;
	}

	showError("格式錯誤，請輸入文章 ID 或網址");
	ga("send", "event", GA_CATEGORY, "generate with error", postInput);
}

function onPostInputKeyPress(e) {
	if (e.keyCode == 13) {
		generate();
	}
}

function generateWithId(postId) {
	imageIndex = 0;

	window.postId = postId;
	showHint("正在載入文章 " + postId + "……");

	postUrl = "https://www.dcard.tw/f/all/p/" + postId + "?ref=embed";
	var apiUrl = "https://www.dcard.tw/_api/posts/" + postId;
	httpGet(apiUrl,
		function onSuccess(responseText) {
			ga("event", "event", GA_CATEGORY, "response success", postId);
			post = JSON.parse(responseText);
			populatePost();
		},
		function onFailure(statusCode, responseText) {
			ga("event", "event", GA_CATEGORY, "response error", statusCode);
			if (statusCode == 404) {
				showError("找不到文章，文章可能被刪除或不存在。");
			} else {
				showError("發生錯誤，請稍後再試（" + statusCode + ", " + responseText + "）");
			}
		}
	);
}

function toggleImageCheckbox() {
	var checkbox = document.getElementById("image_checkbox");
	checkbox.checked = !checkbox.checked;
}

function addImageIndex(indexIncrement) {
	var result = imageIndex + indexIncrement;
	if (result >= 0 && result < mediaSize) {
		imageIndex = result;
		populatePost();
	}
}

function populatePost() {
	ga("event", "event", GA_CATEGORY, "render embed", postId);

	mediaSize = post.media.length;

	// Image controls
	var imageCheckbox = document.getElementById("image_checkbox");
	var hasImage = mediaSize > 0;
	var showImage = imageCheckbox.checked && hasImage;
	document.getElementById("generated_image_settings").style.display = hasImage ? "block" : "none";
	document.getElementById("image_index").innerText = (imageIndex + 1) + "/" + mediaSize;
	document.getElementById("image_chooser").style.display = showImage ? "block" : "none";

	getWidgetHeight(showImage, post.media[0].url, function(height) {
		var currentUrl = window.location.href;
		var path = currentUrl.substring(0, currentUrl.lastIndexOf("/") + 1) + "embed.html";
		var showMediaQuery = showImage ? "showMedia=1" : "";
		var mediaIndexQuery = showImage && (imageIndex > 0) ? "mediaIndex=" + imageIndex : "";
		var code = "<iframe src=\"" + path + "?postId=" + window.postId + "&" + showMediaQuery + "&" + mediaIndexQuery + "\" height=\"" + height + "\" style=\"width:100%;border:none;overflow:hidden\" scrolling=\"no\" frameborder=\"0\" allowTransparency=\"true\"></iframe>";

		// Generate embed
		document.getElementById("embed_container").innerHTML = code;
		var codeTextArea = document.getElementById("code_container");
		codeTextArea.value = code;

		// Show generated area
		document.getElementById("generated_content").style.display = "block";
		dismissMessage();
		adjustScrollableHeight(codeTextArea);
	});
}

function getWidgetHeight(showImage, imageUrl, callback) {
	if (!showImage) {
		callback(COMPACT_WIDGET_SIZE);
	} else {
		var img = new Image();
		img.onload = function() {
			var height = img.height;
			var width = img.width;
			var mediaHeight = MAX_WIDGET_WIDTH * height / width;
			if (mediaHeight > MAX_MEDIA_HEIGHT) {
				mediaHeight = MAX_MEDIA_HEIGHT;
			}
			callback(COMPACT_WIDGET_SIZE + HEADER_MARGIN + mediaHeight);
		}
		img.onerror = function() {
			callback(MAX_WIDGET_HEIGHT);
		}
		img.src = imageUrl;
	}
}

function adjustScrollableHeight(element) {
	var adjustedHeight = element.clientHeight;
	adjustedHeight = Math.max(element.scrollHeight, adjustedHeight);
	if (adjustedHeight > element.clientHeight) {
		element.style.height = (adjustedHeight + 4) + 'px';
	}
}

function copyCode() {
	ga('send', 'event', GA_CATEGORY, "copy code", window.postId);
	document.getElementById("code_container").select();
	document.execCommand("copy");
	document.getElementById("copy_code_button").innerText = "已複製";
}

function resetCopyButton() {
	document.getElementById("copy_code_button").innerText = "複製代碼";
}

function showHint(hintText) {
	document.getElementById("message_container").innerHTML =
		"<span style=\"color:rgba(0,0,0,.54);\">" + hintText + "</span>";
	document.getElementById("message_container").style.display = "block";
}

function showMessage(messageText) {
	document.getElementById("message_container").innerHTML =
		"<span style=\"color:rgba(0,0,0,.87);\">" + messageText + "</span>";
	document.getElementById("message_container").style.display = "block";
}

function showError(errorText) {
	document.getElementById("message_container").innerHTML =
		"<span style=\"color:#f44336;\">" + errorText + "</span>";
	document.getElementById("message_container").style.display = "block";
}

function dismissMessage() {
	document.getElementById("message_container").style.display = "none";
}


function httpGet(url, onSuccess, onFailure) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == XMLHttpRequest.DONE) {
			if (xmlHttp.status == 200) {
				onSuccess(xmlHttp.responseText);
			} else {
				onFailure(xmlHttp.status, xmlHttp.responseText);
			}
		}
	}
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function getParameterByName(name) {
	var url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
	var results = regex.exec(url);
	if (!results) {
		return null
	}
	if (!results[2]) {
		return ''
	}
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}