window.postId = null;
window.postUrl = null;

window.onload = function () {
	window.postId = getParameterByName("postId");
	if (!window.postId) {
		document.getElementsByTagName("body")[0].innerText = "Error: Undefined postId.";
		return;
	}

	window.postUrl = "https://www.dcard.tw/f/all/p/" + window.postId + "?ref=embed";
	var apiUrl = "https://www.dcard.tw/_api/posts/" + window.postId;

	var showMedia = stringToBoolean(getParameterByName("showMedia"));
	var mediaView = document.getElementById("media");
	mediaView.href = window.postUrl;
	mediaView.style.display = showMedia ? "block" : "none";
	if (!showMedia) {
		document.getElementsByTagName("body")[0].style.height = "auto";
	}

	httpGet(apiUrl,
		function onSuccess(responseText) {
			populatePost(JSON.parse(responseText));
		},
		function onFailure(statusCode, responseText) {
			populatePost(null);
			var titleView = document.getElementById("title");
			var contentView = document.getElementById("content");
			if (statusCode == 404) {
				title.innerText = "找不到文章";
				contentView.innerHTML = getNotFoundMessage(JSON.parse(responseText).reason);
			} else {
				title.innerText = statusCode;
				contentView.innerText = responseText;
			}
		}
	);
}

function populatePost(post) {
	// Set up post links
	var titleView = document.getElementById("title");
	titleView.href = window.postUrl;

	document.getElementById("post_link").href = window.postUrl;

	if (!post) {
		return;
	}

	// Bind post data
	var avatarView = document.getElementById("avatar");
	var authorView = document.getElementById("author");
	var personaIdView = document.getElementById("persona_id");
	if (post.school) {
		authorView.innerText = post.school;
	} else {
		authorView.innerText = "匿名";
	}
	if (post.withNickname) {
		if (post.gender == "M" || post.gender == "m") {
			avatarView.style.backgroundColor = "#3d90bf";
		} else if (post.gender == "F" || post.gender == "f") {
			avatarView.style.backgroundColor = "#cb3a6b";
		} else {
			avatarView.style.backgroundColor = "#6c8697";
		}
		if (post.school.length > 0) {
			avatarView.innerText = post.department.substring(0, 1).toUpperCase();
		}
		personaIdView.innerText = "@" + post.department;
	} else {
		if (post.gender == "M" || post.gender == "m") {
			avatarView.style.backgroundImage = "url('res/img/ic_avatar_male.svg')";
		} else if (post.gender == "F" || post.gender == "f") {
			avatarView.style.backgroundImage = "url('res/img/ic_avatar_female.svg')";
		} else {
			avatarView.style.backgroundImage = "url('res/img/ic_avatar_neutral.svg')";
		}
		if (post.department) {
			authorView.innerText += " " + post.department
		}
		personaIdView.style.display = "none";
	}

	titleView.innerText = post.title;
	var forumView = document.getElementById("forum");
	forumView.innerText = post.forumName;

	var contentView = document.getElementById("content");
	contentView.innerText = post.content.replace(/\n+/g, "\n").replace(/\s+$/g, '');;

	if (post.media.length > 0) {
		var mediaIndex = 0
		var mediaIndexParam = parseInt(getParameterByName("mediaIndex"));
		if (mediaIndexParam && mediaIndexParam < post.media.length) {
			mediaIndex = mediaIndexParam;
		}
		var mediaView = document.getElementById("media");
		mediaView.style.backgroundImage = "url(" + post.media[mediaIndex].url + ")";
	}
}

function stringToBoolean(string) {
	if (string == null) {
		return false;
	}
	switch (string.toLowerCase().trim()) {
		case "true":
		case "yes":
		case "1": {
			return true;
		}
		case "false":
		case "no":
		case "0":
		case null: {
			return false;
		}
		default: {
			return Boolean(string)
		}
	}
}

function getNotFoundMessage(reason) {
	var reasonDescription;
	switch (reason) {
		case '': {
			return '這篇文章被作者刪除了';
		}
		case 'insult': {
			reasonDescription = '中傷、歧視、挑釁或謾罵他人';
			break;
		}
		case 'personal': {
			reasonDescription = '交換個人資料';
			break;
		}
		case 'wash': {
			reasonDescription = '惡意洗板、重複張貼';
			break;
		}
		case 'sex': {
			reasonDescription = '色情露點、性行為或血腥恐怖等讓人不舒服之內容';
			break;
		}
		case 'advertise': {
			reasonDescription = '廣告、商業宣傳之內容';
			break;
		}
		case 'uid': {
			reasonDescription = '卡稱或 ID 內含不適當的字詞';
			break;
		}
		case 'wronghole': {
			reasonDescription = '文章發錯板';
			break;
		}
		case 'uncategorized': {
			reasonDescription = '標題未分類';
			break;
		}
		case 'other': {
			reasonDescription = '其他違規及違法的項目';
			break;
		}
		case 'appealed': {
			reasonDescription = '使用者提出申訴';
			break;
		}
		default: {
			reasonDescription = '其他原因';
		}
	}
	return '這篇文章因為<b>' + reasonDescription + '</b>被刪除了。';
}

function httpGet(url, onSuccess, onFailure) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function () {
		if (xmlHttp.readyState == XMLHttpRequest.DONE) {
			if (xmlHttp.status == 200) {
				onSuccess(xmlHttp.responseText);
			} else {
				onFailure(xmlHttp.status, xmlHttp.responseText);
			}
		}
	}
	xmlHttp.open("GET", url, true);
	xmlHttp.send(null);
}

function getParameterByName(name) {
	var url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
	var results = regex.exec(url);
	if (!results) {
		return null
	}
	if (!results[2]) {
		return ''
	}
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}